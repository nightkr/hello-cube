use crate::hitscan::PlayerShooter;
use crate::targets::TargetGeneratorConfig;
use crate::{resources::Resources, score::init_score};
use amethyst::{
    assets::{PrefabData, ProgressCounter},
    controls::FlyControlTag,
    core::{
        math::{Unit, UnitQuaternion, Vector3},
        Float, Time, Transform,
    },
    derive::PrefabData,
    ecs::{Component, Entity, Join, NullStorage, Read, ReadStorage, System, WriteStorage},
    input::{InputHandler, StringBindings},
    prelude::*,
    renderer::camera::CameraPrefab,
    Error,
};
use serde_derive::{Deserialize, Serialize};

pub struct Walker {
    res: Resources,
}

impl Walker {
    pub fn new(res: Resources) -> Self {
        Walker { res }
    }
}

impl SimpleState for Walker {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        init_camera(&self.res, data.world);
        init_score(&self.res, data.world);
        data.world.write_resource::<TargetGeneratorConfig>().res = Some(self.res.clone());
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.write_resource::<TargetGeneratorConfig>().res = None;
    }
}

fn init_camera(res: &Resources, world: &mut World) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, 0.0, 5.0);
    world
        .create_entity()
        .with(res.prefabs.player.clone())
        .with(Movable)
        .with(FlyControlTag)
        .with(PlayerShooter)
        .build();
}

#[derive(Clone, Debug, Deserialize, Serialize, PrefabData)]
pub struct PlayerPrefab {
    camera: CameraPrefab,
    transform: Transform,
}

#[derive(Default, Debug, Deserialize, Serialize)]
pub struct Movable;

impl Component for Movable {
    type Storage = NullStorage<Self>;
}

pub struct MoveSystem;

fn project_flat_unit(
    direction: Vector3<Float>,
    rotation: UnitQuaternion<Float>,
) -> Unit<Vector3<Float>> {
    let mut rotated = rotation * direction;
    rotated.y = Float::from(0.0);
    Unit::new_normalize(rotated)
}

impl<'s> System<'s> for MoveSystem {
    type SystemData = (
        ReadStorage<'s, Movable>,
        WriteStorage<'s, Transform>,
        Read<'s, InputHandler<StringBindings>>,
        Read<'s, Time>,
    );

    fn run(&mut self, (movables, mut transforms, input, time): Self::SystemData) {
        for (_movable, transform) in (&movables, &mut transforms).join() {
            let transform: &mut Transform = transform;

            if let Some(forward_movement) = input.axis_value("forward") {
                let forward_unit = project_flat_unit(
                    Vector3::new(Float::from(0.0), Float::from(0.0), Float::from(1.0)),
                    transform.isometry().rotation,
                );
                transform.prepend_translation_along(
                    forward_unit,
                    forward_movement * time.delta_seconds() as f64 * -20.0,
                );
            }
            if let Some(strafe_movement) = input.axis_value("strafe") {
                let strafe_unit = project_flat_unit(
                    Vector3::new(Float::from(1.0), Float::from(0.0), Float::from(0.0)),
                    transform.isometry().rotation,
                );
                transform.prepend_translation_along(
                    strafe_unit,
                    strafe_movement * time.delta_seconds() as f64 * 20.0,
                );
            }
        }
    }
}
