use crate::{hitscan::HitscanTarget, resources::Resources};
use amethyst::{
    core::{
        math::{Point3, Unit, Vector3},
        Float, Transform,
    },
    ecs::{Entities, LazyUpdate, Read, ReadStorage, System},
    prelude::*,
};
use rand::{thread_rng, Rng};

#[derive(Default)]
pub struct TargetGeneratorConfig {
    pub res: Option<Resources>,
}

pub struct TargetGeneratorSystem;

fn random_point_in_circle(origin: Point3<Float>, radius: Float) -> Point3<Float> {
    let mut rng = thread_rng();
    let distance = Float::from(rng.gen_range(0.0, radius.as_f64()));
    let direction = Unit::new_normalize(Vector3::<Float>::new(
        rng.gen::<f64>().into(),
        0.0.into(),
        rng.gen::<f64>().into(),
    ));
    let point = *direction * distance;
    origin + point
}

impl<'s> System<'s> for TargetGeneratorSystem {
    type SystemData = (
        Entities<'s>,
        Read<'s, TargetGeneratorConfig>,
        Read<'s, LazyUpdate>,
        ReadStorage<'s, HitscanTarget>,
    );

    fn run(&mut self, (entities, config, lazy_update, targets): Self::SystemData) {
        if let Some(ref res) = config.res {
            if targets.is_empty() {
                for _i in 0..10 {
                    lazy_update
                        .create_entity(&entities)
                        .with(HitscanTarget)
                        .with(res.models.cube.clone())
                        .with(Transform::from(
                            random_point_in_circle(Point3::origin(), 10.0.into()).coords,
                        ))
                        .build();
                }
            }
        }
    }
}
