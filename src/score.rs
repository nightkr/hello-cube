use crate::resources::Resources;
use amethyst::{
    ecs::{Entity, Read, System, WriteStorage},
    prelude::*,
    ui::{Anchor, UiText, UiTransform},
};

pub fn init_score(res: &Resources, world: &mut World) {
    let label = world
        .create_entity()
        .with(UiTransform::new(
            "score".to_string(),
            Anchor::TopLeft,
            Anchor::TopLeft,
            0.0,
            0.0,
            1.0,
            200.0,
            100.0,
        ))
        .with(UiText::new(
            res.fonts.main.clone(),
            "".to_string(),
            [1.0, 1.0, 1.0, 1.0],
            50.,
        ))
        .build();
    world.add_resource(ScoreText { label });
}

pub struct ScoreText {
    label: Entity,
}

#[derive(Default)]
pub struct Score {
    pub score: i32,
}

pub struct ScoreRenderSystem;

impl<'s> System<'s> for ScoreRenderSystem {
    type SystemData = (
        Read<'s, Score>,
        Option<Read<'s, ScoreText>>,
        WriteStorage<'s, UiText>,
    );

    fn run(&mut self, (score, score_text, mut ui_text): Self::SystemData) {
        if let Some(score_text) = score_text {
            if let Some(label) = ui_text.get_mut(score_text.label) {
                label.text = format!("Score: {}", score.score);
            }
        }
    }
}
