use amethyst::{
    assets::{Completion, Handle, Loader, Prefab, ProgressCounter, RonFormat},
    gltf::{GltfSceneAsset, GltfSceneFormat},
    prelude::*,
    ui::{FontAsset, TtfFormat},
};

#[derive(Clone)]
pub struct Models {
    pub cube: Handle<GltfSceneAsset>,
}

#[derive(Clone)]
pub struct Prefabs {
    pub player: Handle<Prefab<crate::walker::PlayerPrefab>>,
}

#[derive(Clone)]
pub struct Fonts {
    pub main: Handle<FontAsset>,
}

#[derive(Clone)]
pub struct Resources {
    pub models: Models,
    pub prefabs: Prefabs,
    pub fonts: Fonts,
}

pub struct Loading<DoneFn> {
    res: Option<Resources>,
    progress: ProgressCounter,
    done: DoneFn,
}

impl<DoneFn: Fn(Resources) -> Box<dyn State<GameData<'static, 'static>, StateEvent>>>
    Loading<DoneFn>
{
    pub fn new(done: DoneFn) -> Self {
        Loading {
            res: None,
            progress: ProgressCounter::new(),
            done,
        }
    }
}

impl<DoneFn: Fn(Resources) -> Box<dyn State<GameData<'static, 'static>, StateEvent>>> SimpleState
    for Loading<DoneFn>
{
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let res = {
            let loader = data.world.read_resource::<Loader>();
            Resources {
                models: Models {
                    cube: loader.load(
                        "cube.glb",
                        GltfSceneFormat::default(),
                        &mut self.progress,
                        &data.world.read_resource(),
                    ),
                },
                prefabs: Prefabs {
                    player: loader.load(
                        "player.prefab.ron",
                        RonFormat,
                        &mut self.progress,
                        &data.world.read_resource(),
                    ),
                },
                fonts: Fonts {
                    main: loader.load(
                        "DejaVuSans.ttf",
                        TtfFormat,
                        &mut self.progress,
                        &data.world.read_resource(),
                    ),
                },
            }
        };

        self.res = Some(res);
    }

    fn update(&mut self, _data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        match self.progress.complete() {
            Completion::Loading => Trans::None,
            Completion::Complete => Trans::Switch((self.done)(self.res.as_ref().unwrap().clone())),
            Completion::Failed => panic!("Failed to load resources: {:#?}", self.progress.errors()),
        }
    }
}
