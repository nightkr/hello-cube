use crate::score::Score;
use amethyst::{
    core::{
        math::{Point3, Vector3},
        Float, ParentHierarchy, Transform,
    },
    ecs::{Component, Entities, Join, NullStorage, Read, ReadExpect, ReadStorage, System, Write},
    input::{InputHandler, StringBindings},
    renderer::visibility::BoundingSphere,
};
use ncollide3d::{
    query::Ray,
    shape::{Ball, ShapeHandle},
    world::{CollisionGroups, CollisionWorld, GeometricQueryType},
};
use std::cmp::Ordering;

#[derive(Default)]
pub struct HitscanTarget;

impl Component for HitscanTarget {
    type Storage = NullStorage<Self>;
}

// pub struct Hitbox {
//     bounding_box: AABB<Float>,
// }

// impl Component for Hitbox {
//     type Storage = DenseVecStorage<Self>;
// }

// pub struct GltfHitboxBuilderSystem;

// impl<'s> System<'s> for GltfHitboxBuilderSystem {
//     type SystemData = (
//         ReadStorage<'s, BoundingSphere>,
//         ReadStorage<'s, Transform>,
//         ReadStorage<'s, HitscanTarget>,
//     );

//     fn run(&mut self, (bounding_spheres, transforms, targets): Self::SystemData) {
//         for (bounding_sphere, transform, target) in
//             (&bounding_spheres, &transforms, &targets).join()
//         {
//             dbg!("entity");
//         }
//     }
// }

#[derive(Default)]
pub struct PlayerShooter;

impl Component for PlayerShooter {
    type Storage = NullStorage<Self>;
}

#[derive(Default)]
pub struct HitscanSystem {
    firing: bool,
}

impl<'s> System<'s> for HitscanSystem {
    type SystemData = (
        Entities<'s>,
        ReadExpect<'s, ParentHierarchy>,
        ReadStorage<'s, PlayerShooter>,
        ReadStorage<'s, Transform>,
        ReadStorage<'s, HitscanTarget>,
        ReadStorage<'s, BoundingSphere>,
        Read<'s, InputHandler<StringBindings>>,
        Write<'s, Score>,
    );

    fn run(
        &mut self,
        (entities, hierarchy, shooters, transforms, targets, bounding_spheres, input, mut score): Self::SystemData,
    ) {
        let fire_pressed = input.action_is_down("fire").unwrap();
        if fire_pressed && !self.firing {
            // TODO: Persist the world across frames
            let mut world = CollisionWorld::new(Float::from(0.02));
            let mut targets_group = CollisionGroups::new();
            targets_group.set_membership(&[0]);
            for (target_entity, transform, _target) in (&entities, &transforms, &targets).join() {
                // TODO: Generate better shapes for the whole box, and handle stuff that isn't centered at (0,0,0) from the parent
                for (_child, bounding_sphere) in
                    (hierarchy.all_children(target_entity), &bounding_spheres).join()
                {
                    let shape_handle = ShapeHandle::new(Ball::new(bounding_sphere.radius));
                    world.add(
                        *transform.isometry(),
                        shape_handle,
                        targets_group,
                        GeometricQueryType::Proximity(0.02.into()),
                        target_entity,
                    );
                }
            }
            world.update();
            for (_shooter, transform) in (&shooters, &transforms).join() {
                let isometry = transform.isometry();
                let origin = isometry.translation * Point3::origin();
                let direction =
                    isometry.rotation * -Vector3::new(0.0.into(), 0.0.into(), 1.0.into());
                let target = world
                    .interferences_with_ray(&Ray::new(origin, direction), &targets_group)
                    .min_by(|(_, intersection1), (_, intersection2)| {
                        intersection1
                            .toi
                            .partial_cmp(&intersection2.toi)
                            .unwrap_or(Ordering::Less)
                    });
                if let Some((target, _intersection)) = target {
                    score.score += 1;
                    entities.delete(*target.data()).unwrap();
                }
            }
        }
        self.firing = fire_pressed;
    }
}
