use amethyst::{
    assets::PrefabLoaderSystem,
    controls::{CursorHideSystem, FreeRotationSystem, MouseFocusUpdateSystem},
    core::TransformBundle,
    ecs::{ReadExpect, Resources},
    gltf::GltfSceneLoaderSystem,
    input::{InputBundle, StringBindings},
    prelude::*,
    renderer::{
        pass::{DrawFlatDesc, DrawSkyboxDesc},
        types::DefaultBackend,
        Factory, Format, GraphBuilder, GraphCreator, Kind, RenderGroupDesc, RenderingSystem,
        SubpassBuilder,
    },
    shred::SystemData,
    ui::{DrawUiDesc, UiBundle},
    utils::application_root_dir,
    window::{ScreenDimensions, Window, WindowBundle},
};

mod hitscan;
mod resources;
mod score;
mod targets;
mod walker;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = application_root_dir()?
        .join("resources")
        .join("display_config.ron");

    let game_data = GameDataBuilder::default()
        .with_bundle(WindowBundle::from_config_path(path))?
        .with_bundle(TransformBundle::new())?
        .with_bundle(
            InputBundle::<StringBindings>::new().with_bindings_from_file(
                application_root_dir()?
                    .join("resources")
                    .join("bindings_config.ron"),
            )?,
        )?
        .with_bundle(UiBundle::<DefaultBackend, StringBindings>::new())?
        .with_thread_local(RenderingSystem::<DefaultBackend, _>::new(
            RenderGraph::default(),
        ))
        .with(
            GltfSceneLoaderSystem::default(),
            "gltf_scene_loader_system",
            &[],
        )
        .with(
            PrefabLoaderSystem::<walker::PlayerPrefab>::default(),
            "player_loader_system",
            &[],
        )
        .with(
            MouseFocusUpdateSystem::new(),
            "mouse_focus_system",
            &["input_system"],
        )
        .with(
            FreeRotationSystem::new(0.1, 0.1),
            "camera_rotation_system",
            &["input_system", "mouse_focus_system"],
        )
        .with(
            CursorHideSystem::new(),
            "cursor_hide_system",
            &["input_system", "camera_rotation_system"],
        )
        .with(
            walker::MoveSystem,
            "move_system",
            &["input_system", "camera_rotation_system"],
        )
        .with(
            hitscan::HitscanSystem::default(),
            "hitscan_system",
            &["move_system", "input_system"],
        )
        .with(
            targets::TargetGeneratorSystem,
            "target_generator_system",
            &["hitscan_system"],
        )
        .with(
            score::ScoreRenderSystem,
            "score_render_system",
            &["hitscan_system"],
        );
    let mut game = Application::new(
        "resources/",
        resources::Loading::new(|res| Box::new(walker::Walker::new(res))),
        game_data,
    )?;

    game.run();

    Ok(())
}

#[derive(Default)]
struct RenderGraph {
    dimensions: Option<ScreenDimensions>,
    dirty: bool,
}

impl GraphCreator<DefaultBackend> for RenderGraph {
    // This trait method reports to the renderer if the graph must be rebuilt, usually because
    // the window has been resized. This implementation checks the screen size and returns true
    // if it has changed.
    fn rebuild(&mut self, res: &Resources) -> bool {
        // Rebuild when dimensions change, but wait until at least two frames have the same.
        let new_dimensions = res.try_fetch::<ScreenDimensions>();
        use std::ops::Deref;
        if self.dimensions.as_ref() != new_dimensions.as_ref().map(|d| d.deref()) {
            self.dirty = true;
            self.dimensions = new_dimensions.map(|d| d.clone());
            return false;
        }
        return self.dirty;
    }

    // This is the core of a RenderGraph, which is building the actual graph with subpasses and target
    // images.
    fn builder(
        &mut self,
        factory: &mut Factory<DefaultBackend>,
        res: &Resources,
    ) -> GraphBuilder<DefaultBackend, Resources> {
        use amethyst::renderer::rendy::{
            graph::present::PresentNode,
            hal::command::{ClearDepthStencil, ClearValue},
        };

        self.dirty = false;

        // Retrieve a reference to the target window, which is created by the WindowBundle
        let window = <ReadExpect<'_, Window>>::fetch(res);
        let dimensions = self.dimensions.as_ref().unwrap();
        let window_kind = Kind::D2(dimensions.width() as u32, dimensions.height() as u32, 1, 1);

        // Create a new drawing surface in our window
        let surface = factory.create_surface(&window);
        let surface_format = factory.get_surface_format(&surface);

        // Begin building our RenderGraph
        let mut graph_builder = GraphBuilder::new();
        let color = graph_builder.create_image(
            window_kind,
            1,
            surface_format,
            // clear screen to black
            Some(ClearValue::Color([0.2, 0.2, 0.8, 1.0].into())),
        );

        let depth = graph_builder.create_image(
            window_kind,
            1,
            Format::D32Sfloat,
            Some(ClearValue::DepthStencil(ClearDepthStencil(1.0, 0))),
        );

        // Create our single `Subpass`, which is the DrawFlat2D pass.
        // We pass the subpass builder a description of our pass for construction
        let pass = graph_builder.add_node(
            SubpassBuilder::new()
                .with_group(DrawSkyboxDesc::new().builder())
                .with_group(DrawFlatDesc::new().builder())
                .with_group(DrawUiDesc::new().builder())
                .with_color(color)
                .with_depth_stencil(depth)
                .into_pass(),
        );

        // Finally, add the pass to the graph
        let _present = graph_builder
            .add_node(PresentNode::builder(factory, surface, color).with_dependency(pass));

        graph_builder
    }
}
